﻿using UnityEngine;
using System.Collections;

public class AsteriodScript : Percepts {
	GameObject attractor;
	SphereCollider col;
	// Use this for initialization
	void Start () {
		col = GetComponent<SphereCollider> ();
	}
	
	// Update is called once per frame
	void Update () {
		attractor = GetComponent<ListOfPlanets> ().closest.GetComponent<GravityAttractor>().gameObject;
		//Debug.Log (GetDistanceToObject (attractor));
		hit ();
	}

	void hit(){
		if (GetDistanceToObject (attractor)<=35) {
			Debug.Log ("hit");
			GameObject.FindWithTag("TheWorld").GetComponent<worldHealth>().health-=50;
			GameObject.FindWithTag ("rifle").GetComponent<rifle> ().boom ();
			Destroy(this.gameObject);
		}
	}
}
