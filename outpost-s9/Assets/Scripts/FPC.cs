﻿using UnityEngine;
using System.Collections;

public class FPC : MonoBehaviour {

	public bool move;
	public float mouseSensitivityX = 3.5f;
	public float mouseSensitivityY = 3.5f;
	public float walkSpeed = 8f;
	public bool canBoost;
	public bool boosting;
	public float boostGauge = 1000f;
	public float jumpForce = 220f;
	public float jetForce = 220f;
	float downForce = 1000;
	public LayerMask groundedMask;

	Transform cameraT;
	float verticalLookRotation;

	Vector3 moveAmount;
	Vector3 smoothMoveVelocity;

	bool grounded;

	// Use this for initialization
	void Start () {
		Screen.lockCursor = false;
		cameraT = Camera.main.transform;
		move = true;
		canBoost = true;
		boosting = false;
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log("boostGauge is " +boostGauge+ " canBoost is "+canBoost);
		if (boostGauge == 0f){
			canBoost = false;
			move = true;
		}

		if (canBoost == false && boostGauge == 100f){
			canBoost = true;
		}

		if (canBoost == true){
			if (Input.GetButton("Fire3") || move == false || Input.GetKeyDown("r") || Input.GetKeyDown("e")){
				Debug.Log("boosting");
				boosting = true;
				boostGauge--;
			} else{
				Debug.Log("no boost");
				boosting = false;
				if (boostGauge <= 999f && boosting == false){
					Debug.Log("not full");
					boostGauge++;
				}
			}
		} else{
			Debug.Log("empty");
			boostGauge++;
		}

		if (GetComponent<Pause> ().paused == false) {
			transform.Rotate (Vector3.up * Input.GetAxis ("Mouse X") * mouseSensitivityX);
			verticalLookRotation += Input.GetAxis ("Mouse Y") * mouseSensitivityY;
			verticalLookRotation = Mathf.Clamp (verticalLookRotation, -80, 80);
			cameraT.localEulerAngles = Vector3.left * verticalLookRotation;

			Vector3 moveDir = new Vector3 (Input.GetAxisRaw ("Horizontal"), 0, Input.GetAxisRaw ("Vertical")).normalized;
			Vector3 targetMoveAmount = moveDir * walkSpeed;

			moveAmount = Vector3.SmoothDamp (moveAmount, targetMoveAmount, ref smoothMoveVelocity, .15f);

			if (Input.GetButtonDown ("Jump")) {
				if (grounded) {
					GetComponent<Rigidbody> ().AddForce (transform.up * jumpForce);
					move = true;
				}
			}

			if (Input.GetButton ("Fire3")&&canBoost==true) {
				GetComponent<Rigidbody> ().AddForce (GameObject.FindWithTag ("MainCamera").transform.forward * jetForce);
				move = true;
			}

			if (Input.GetKeyDown ("r")&&canBoost==true) {
				GetComponent<Rigidbody> ().velocity = Vector3.zero;
				GetComponent<Rigidbody> ().AddForce (-transform.up * downForce);
				move = true;
			}

			if (Input.GetKeyDown ("e")&&canBoost==true) {
				Debug.Log ("stopping");
				move = false;
			}

			if (move == true) {
				Debug.Log ("moving");
			} else {
				GetComponent<Rigidbody> ().velocity = Vector3.zero;
			}

			grounded = false;
			Ray ray = new Ray (transform.position, -transform.up);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, 1 + .1f, groundedMask)) {
				grounded = true;
			}
		}
	}

	void FixedUpdate(){
		GetComponent<Rigidbody> ().MovePosition (GetComponent<Rigidbody> ().position 
			+ transform.TransformDirection (moveAmount) * Time.fixedDeltaTime);
	}
}
