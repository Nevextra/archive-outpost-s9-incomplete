﻿using UnityEngine;
using System.Collections;


public class GlobalSettings : MonoBehaviour
{
    public float sound;
    public float music;
    public Resolution resolution;
    public bool fullscreen;

    public static GlobalSettings _instance;

    void Awake()
    {
        if (!_instance)
        {
            _instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
}
