﻿using UnityEngine;
using System.Collections;

public class GravityAttractor : Percepts {
	public float gravity = -9.8f;
	public float lenght;
	// Use this for initialization
	void Awake () {
		base.Awake ();
		PrimeAttractors ();
	}
	
	// Update is called once per frame
	void Update () {
		lenght = GetDistanceToPlayer ();
	}

	public void Attract(Transform body){
		Vector3 gravityUp = (body.position - transform.position).normalized;
		Vector3 bodyUp = body.up;

		body.GetComponent<Rigidbody> ().AddForce (gravityUp * gravity);

		body.rotation = Quaternion.FromToRotation (bodyUp, gravityUp) * body.rotation;
		//body.rotation = Quaternion.Slerp (body.rotation, targetRotation,50 * Time.deltaTime);
	}
}
