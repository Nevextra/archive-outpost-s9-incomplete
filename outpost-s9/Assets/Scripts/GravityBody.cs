﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class GravityBody : Percepts {
	public GravityAttractor attractor;

	// Use this for initialization
	void Awake () {
		base.Awake ();
		PrimePlanet ();
	}
	
	// Update is called once per frame
	void FixedUpdate(){
		attractor = GetComponent<ListOfPlanets> ().closest.GetComponent<GravityAttractor>();
		attractor.Attract (transform);


	}


}
