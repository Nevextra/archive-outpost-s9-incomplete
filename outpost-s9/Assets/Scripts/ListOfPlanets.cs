﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ListOfPlanets : MonoBehaviour {
	public List<GameObject> planets = new List<GameObject> ();
	public float current;
	public GameObject closest;
	// Use this for initialization
	void Start () {
		closest = GameObject.FindWithTag ("Planet");
	}
	
	// Update is called once per frame
	void Update () {
		current = closest.GetComponent<GravityAttractor>().lenght;
		sort ();
	}

	void sort (){
		float dis;
		for(int i = 0; i<planets.Count;i++){
			dis = planets[i].GetComponent<GravityAttractor>().lenght;
			if(!planets.Contains(closest)){
				//Debug.Log("logged");
				closest = planets[i];
			}
			if(dis<current){
				closest = planets[i];
			}
			//Debug.Log(closest);
		}
	}
}
