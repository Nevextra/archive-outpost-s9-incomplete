﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class MenuController : MonoBehaviour
{
	public AudioSource[] AudioClip = null;
    public string levelToLoad;

    public float cameraRotation;
    public float speed;

    public Dropdown resolutionDropdown;
    public Toggle fullscreenToggle;
    public Slider soundSlider;
    public Slider musicSlider;

    Resolution[] resolutions;

	// Use this for initialization
	void Start ()
    {
		Time.timeScale = 1;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        resolutions = Screen.resolutions;

        List<string> resolutionStrings = new List<string>();

        for (int i = 0; i < resolutions.Length; i++)
        {
            resolutionStrings.Add(resolutions[i].ToString());
        }

        resolutionDropdown.ClearOptions();
        resolutionDropdown.AddOptions(resolutionStrings);

        resolutionDropdown.value = resolutionStrings.FindIndex(x => x.StartsWith(Screen.currentResolution.ToString()));

        GlobalSettings._instance.sound = soundSlider.value;
        GlobalSettings._instance.music = musicSlider.value;
        GlobalSettings._instance.resolution = Screen.currentResolution;
        GlobalSettings._instance.fullscreen = Screen.fullScreen;

    //    public float sound;
    //public float music;
    //public Resolution resolution;
    //public bool fullscreen;

        Debug.Log("Menu Loaded");
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 rot = new Vector3(0, cameraRotation, 0);
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(rot), Time.deltaTime * speed);
	}

    public void toMainMenu()
    {
		GameObject.FindWithTag ("MainCamera").GetComponent<AudioSource> ().volume =
			GameObject.FindWithTag ("SoundSlider").GetComponent<SliderText> ().slider.value;
		AudioClip [0].Play();
        cameraRotation = 0;
    }

    public void toOptionsScreen()
    {
		//Debug.Log (GameObject.FindWithTag ("MainCamera").GetComponent<AudioSource> ().volume);
		GameObject.FindWithTag ("MainCamera").GetComponent<AudioSource> ().volume =
			GameObject.FindWithTag ("SoundSlider").GetComponent<SliderText> ().slider.value;
		AudioClip [0].Play();
        cameraRotation = 90;
    }

    public void toCreditsScreen()
    {
		GameObject.FindWithTag("MainCamera").GetComponent<AudioSource>().volume =
			GameObject.FindWithTag ("SoundSlider").GetComponent<SliderText> ().slider.value;
		AudioClip [0].Play();
        cameraRotation = 270;
    }

    public void startGame()
    {
		GameObject.FindWithTag("MainCamera").GetComponent<AudioSource>().volume =
			GameObject.FindWithTag ("SoundSlider").GetComponent<SliderText> ().slider.value;
		AudioClip [0].Play();
        SceneManager.LoadScene(levelToLoad);
    }

    public void exit()
    {
		GameObject.FindWithTag("MainCamera").GetComponent<AudioSource>().volume =
			GameObject.FindWithTag ("SoundSlider").GetComponent<SliderText> ().slider.value;
		AudioClip [0].Play();
        Application.Quit();
    }

    public void resolutionChanged()
    {
        Screen.SetResolution(resolutions[resolutionDropdown.value].width, resolutions[resolutionDropdown.value].height, Screen.fullScreen);
        GlobalSettings._instance.resolution = Screen.currentResolution;
    }

    public void setFullscreen()
    {
        Screen.fullScreen = fullscreenToggle.isOn;
        GlobalSettings._instance.fullscreen = Screen.fullScreen;
    }
}