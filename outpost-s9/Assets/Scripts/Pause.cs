﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour {

	public bool paused;
	public bool gameovered;
	public GameObject pausemenu;
	public GameObject hud;
	public GameObject go;

	// Use this for initialization
	void Start () {
		paused = false;
		gameovered = false;
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetKeyDown ("p")) {
			paused = !paused;
		}
		if (gameovered) {
			Debug.Log("gameover screen");
			Time.timeScale = 0;
			Cursor.visible = true;
			Cursor.lockState = CursorLockMode.None;
			pausemenu.SetActive (false);
			hud.SetActive (false);
			go.SetActive(true);
		} else {
			go.SetActive(false);
			if (paused) {
				Time.timeScale = 0;
				Cursor.visible = true;
				Cursor.lockState = CursorLockMode.None;
				pausemenu.SetActive (true);
				hud.SetActive (false);
			} else {
				Time.timeScale = 1;
				Cursor.visible = false;
				Cursor.lockState = CursorLockMode.Confined;
				pausemenu.SetActive (false);
				hud.SetActive (true);
			}
		}
	}
}
