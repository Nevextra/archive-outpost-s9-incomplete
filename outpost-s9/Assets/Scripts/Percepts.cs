﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Percepts : MonoBehaviour {
	public GameObject pl;
	// Use this for initialization
	protected void Awake () {
		pl = GameObject.FindWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
	}

    public bool GetIfPlayerWithinRange(float range)
	{
		return (GetDistanceToPlayer() < range);
	}
	
	public float GetDistanceToPlayer()
	{
		
		float distance=Vector3.Distance(pl.transform.position, transform.position);
		//Debug.Log(distance);
		return distance;
	}

	public float GetDistanceToObject(GameObject thing)
	{
		float distance=Vector3.Distance(thing.transform.position, transform.position);
		//Debug.Log(distance);
		return distance;
	}
	
	public void FacePlayer()
	{
		float angle=GetAngleToPlayer();
		transform.Rotate(0,-GetAngleToPlayer(),0);
	}
	
	public void FaceAwayFromPlayer()
	{
		float angle=180.0f+GetAngleToPlayer();
		transform.Rotate(0,-angle,0);
	}


	public Vector3 GetVectorToObject(GameObject piece)
	{
		Vector3 origin = transform.position;
		Vector3 dir = piece.transform.position;
		Vector3 targetDir = dir - origin;
		return targetDir;

	}
	
	public float GetAngleToPlayer()
	{
		//get relative position of player
		Vector3 targetDir= GetVectorToObject(pl);
		
		//forward vector of NPC
		Vector3 forward = transform.forward;
		forward.y=0;
		//calc cangle between 
		float angle = Vector3.Angle(forward,targetDir);
		
		//is angle positive?
		//compute cross product
		Vector3 cross=Vector3.Cross(targetDir,forward);
		if (cross.y<0) angle *=-1.0f;
		
		return angle;
	}

	public float NpcAngle()
	{
		//get relative position of player
		Vector3 targetDir= GetVectorToObject(pl);
		
		//forward vector of NPC
		Vector3 forward = transform.forward;
		forward.y=0;
		//calc cangle between 
		float angle = Vector3.Angle(forward,targetDir);
		
		return angle;
	}

	public bool PlayerInAngle(float degree)
	{
		return(NpcAngle () < degree);
	}

	public void FacePoint(Vector3 pt)
	{
		float angle=GetAngleToPoint(pt);
		transform.Rotate(0,-angle,0);
	}
	
	public Vector3 GetVectorToPoint(Vector3 pt)
	{
		Vector3 targetDir= pt - transform.position;
		targetDir.y=0;
		return targetDir;
	}
	
	public float GetDistanceToPoint(Vector3 pt)
	{
		
		float distance=Vector3.Distance(pt, transform.position);
		//Debug.Log(distance);
		return distance;
	}
	
	public float GetAngleToPoint(Vector3 pt)
	{
		//get relative position of player
		Vector3 targetDir= GetVectorToPoint(pt);
		
		//forward vector of NPC
		Vector3 forward = transform.forward;
		forward.y=0;
		//calc cangle between 
		float angle = Vector3.Angle(forward,targetDir);
		
		//is angle positive?
		//compute cross product
		Vector3 cross=Vector3.Cross(targetDir,forward);
		if (cross.y<0) angle *=-1.0f;
		return angle;
	}
		

	public bool CanSeeObject(GameObject piece)
	{
		RaycastHit hit;
		Vector3 rayStart = this.transform.position;
		Vector3 rayDirection = this.transform.forward;
		if (Physics.Raycast(rayStart, rayDirection, out hit))
		{
			//Debug.Log("something in front : " + hit.collider.name);
			if (hit.collider.name == piece.name)
				return true;
		}
		return false;
		
	}

	public void PrimePlanet(){
		GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
		GetComponent<Rigidbody>().useGravity = false;
	}

	public void PrimeAttractors(){
		GameObject.FindWithTag("Player").GetComponent<ListOfPlanets> ().planets.Add (gameObject);
	}
}

