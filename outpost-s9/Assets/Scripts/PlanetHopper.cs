﻿using UnityEngine;
using System.Collections;

public class PlanetHopper : MonoBehaviour {

	public bool move;
	public float mouseSensitivityX = 3.5f;
	public float mouseSensitivityY = 3.5f;
	public float walkSpeed = 8f;
	public float jumpForce = 220f;
	float downForce = 1000;
	public LayerMask groundedMask;

	Transform cameraT;
	float verticalLookRotation;

	Vector3 moveAmount;
	Vector3 smoothMoveVelocity;

	public bool grounded;

	// Use this for initialization
	void Start () {
		Screen.lockCursor = true;
		cameraT = Camera.main.transform;
		move = true;
	}
	
	// Update is called once per frame
	void Update () {

		if (GetComponent<Pause> ().paused == false) {
			transform.Rotate (Vector3.up * Input.GetAxis ("Mouse X") * mouseSensitivityX);
			verticalLookRotation += Input.GetAxis ("Mouse Y") * mouseSensitivityY;
			verticalLookRotation = Mathf.Clamp (verticalLookRotation, -80, 80);
			cameraT.localEulerAngles = Vector3.left * verticalLookRotation;

			Vector3 moveDir = new Vector3 (Input.GetAxisRaw ("Horizontal"), 0, Input.GetAxisRaw ("Vertical")).normalized;
			Vector3 targetMoveAmount = moveDir * walkSpeed;

			moveAmount = Vector3.SmoothDamp (moveAmount, targetMoveAmount, ref smoothMoveVelocity, .15f);

			if (Input.GetButtonDown ("Jump")) {
				if (grounded == true) {
					grounded = false;
					GetComponent<Rigidbody> ().AddForce (transform.up * jumpForce);
					move = true;
					Debug.Log ("jumped");
				}
			}

			if (move == true) {
				Debug.Log ("moving");
			} else {
				GetComponent<Rigidbody> ().velocity = Vector3.zero;
			}
			
			if (grounded == false) {
				Debug.Log ("no jumping");
				StartCoroutine (jumpGap ());
			}
		}
	}

	IEnumerator jumpGap(){
		yield return new WaitForSeconds(0.2f);
			Ray ray = new Ray (transform.position, -transform.up);
			RaycastHit hit;
		if (Physics.Raycast (ray, out hit, 1+.1f, groundedMask)) {
			grounded = true;
		}
	}

	void FixedUpdate(){
		GetComponent<Rigidbody> ().MovePosition (GetComponent<Rigidbody> ().position 
			+ transform.TransformDirection (moveAmount) * Time.fixedDeltaTime);
	}
}
