﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ResolutionSetting : MonoBehaviour
{
    Resolution[] resolutions;
    Dropdown dropdown;

	// Use this for initialization
	void Start ()
    {
        resolutions = Screen.resolutions;
        dropdown = GetComponent<Dropdown>();

        List<string> resolutionStrings = new List<string>();

        for (int i = 0; i < resolutions.Length; i++)
        {
            resolutionStrings.Add(resolutions[i].ToString());
            Debug.Log(resolutions[i].ToString());
        }

        dropdown.ClearOptions();
        dropdown.AddOptions(resolutionStrings);

        dropdown.value = resolutionStrings.FindIndex(x => x.StartsWith(Screen.currentResolution.ToString()));
        Debug.Log(Screen.currentResolution);

        
	}
	
	// Update is called once per frame
	void Update ()
    {
	    
	}

    public void resolutionChanged()
    {
        Screen.SetResolution(resolutions[dropdown.value].width, resolutions[dropdown.value].height, Screen.fullScreen);
    }

    public void setFullscreen()
    {
        //Debug.Log("Fullscreen: " + fullscreen);
    }
}
