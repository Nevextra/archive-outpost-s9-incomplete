﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SliderText : MonoBehaviour
{
    public GameObject textObject;
    
    public Slider slider;
    public Text text;

	// Use this for initialization
	void Start ()
    {
        slider = GetComponent<Slider>();
        text = textObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		setText();
		if (this.gameObject.tag == "SoundSlider") {
			GameObject.FindWithTag ("GlobalSettings").GetComponent<GlobalSettings> ().sound = Mathf.Round (slider.value*100);
		}

		if (this.gameObject.tag == "MusicSlider") {
			GameObject.FindWithTag ("GlobalSettings").GetComponent<GlobalSettings> ().music = Mathf.Round (slider.value*100);
		}
	}

    public void setText()
    {
		text.text = Mathf.Round (slider.value*100).ToString();
    }
}
