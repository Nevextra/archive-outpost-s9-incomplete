﻿using UnityEngine;
using System.Collections;

public class boostGauge : MonoBehaviour {
	// Use this for initialization
	public RectTransform dos;
	void Start () {
		dos = GetComponent<RectTransform>();

	}
	
	// Update is called once per frame
	void Update () {
		dos.sizeDelta = new Vector2 (85, GameObject.FindWithTag ("Player").GetComponent<FPC> ().boostGauge / 5);
	}
}
