﻿using UnityEngine;
using System.Collections;

public class gravity : MonoBehaviour {
	public bool forces;
	public static Vector3 Gravitation;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Physics.gravity = Gravitation;
		if (Input.GetKeyDown ("1")) {
			forces = !forces;
			Debug.Log ("switch");
		}
		//Debug.Log (Physics.gravity);
		SwitchGravity();
	}

	public void SwitchGravity(){
		if (forces == true) {
			Gravitation = new Vector3 (0f, -9.8f, 0f);
		} else {
			Gravitation = new Vector3 (0f, 0f, 0f);
		}
	}
}
