﻿using UnityEngine;
using System.Collections;

public class music : MonoBehaviour {
	public AudioSource[] AudioClip = null;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<AudioSource> ().volume = GameObject.FindWithTag ("GlobalSettings").GetComponent<GlobalSettings> ().music/100;

		if (!AudioClip [0].isPlaying) {
			AudioClip [0].Play ();
		}
	}
}
