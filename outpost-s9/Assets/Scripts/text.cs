﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class text : MonoBehaviour
{

	// Use this for initialization
	void Awake ()
    {
        string settingsText = "";

        settingsText += GlobalSettings._instance.sound.ToString() + "\n";
        settingsText += GlobalSettings._instance.music.ToString() + "\n";
        settingsText += GlobalSettings._instance.resolution.ToString() + "\n";
        settingsText += GlobalSettings._instance.fullscreen.ToString();

        Text textComponent = GetComponent<Text>();
        textComponent.text = settingsText;
	}
	
	// Update is called once per frame
	void Update ()
    {
	    
	}
}
