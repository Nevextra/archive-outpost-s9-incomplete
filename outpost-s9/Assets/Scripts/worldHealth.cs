﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class worldHealth : MonoBehaviour {
	public int health;
	public GameObject pausemenu;
	public GameObject hud;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (health <= 0) {
			GameOver();
		}
	}

	public void GameOver(){
		GameObject.FindWithTag ("Player").GetComponent<Pause> ().gameovered = true;
	}
}
